import { FSAAction } from "./util";
import Entry from "../model/entry";


export const DescriptionChangeAction = new FSAAction<string>("DescriptionChangedAction");
export const AddEntryAction     = new FSAAction("AddEntryAction");
export const AmountChangeAction = new FSAAction<number>("AmountChangeAction");
export const RemoveEntryAction  = new FSAAction<number>("RemoveEntryAction");