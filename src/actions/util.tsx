import { Action } from "redux";

export class FSAAction<P = null, M = null> implements Action<string>{

    /* FSA interface */
    public type     : string;
    public payload? : P;
    public error?   : boolean;
    public meta?    : M;

    constructor (type : string){
        this.type = type;
    }

    public is(obj : any){
        return obj && obj.type === this.type;
    }

    public from(obj : any){
        return obj as FSAAction<P, M>;
    }

    public create(payload? : P, error = false, meta? : M){
        return { type : this.type, 
                    error,
                    payload,
                    meta 
                };
    }
}
