import Entry from "../model/entry";
import { EntriesStorage }from "../store/store";
import * as EntriesAction from "../actions/entries";
import { Action } from "redux";


const initialState = {
    entries : new Array<Entry>(),
    currentDescription : '',
    currentAmount      : 0
};


export function entries(state = initialState, action : Action<string>) : EntriesStorage{

    if (EntriesAction.AddEntryAction.is(action)){
        let entry = new Entry(state.currentDescription, state.currentAmount);
        return Object.assign({}, state, {
            entries : new Array<Entry>(...state.entries, entry),
            currentDescription : '',
            currentAmount : 0
        });
    }

    if (EntriesAction.DescriptionChangeAction.is(action)){
        let castedAction = EntriesAction.DescriptionChangeAction.from(action);
        return Object.assign({}, state, {
            currentDescription : castedAction.payload
        });
    }

    if (EntriesAction.AmountChangeAction.is(action)){
        let castedAction = EntriesAction.AmountChangeAction.from(action);
        return Object.assign({}, state, {
            currentAmount : castedAction.payload
        });
    }

    if (EntriesAction.RemoveEntryAction.is(action)){
        let castedAction = EntriesAction.RemoveEntryAction.from(action);
        return Object.assign({}, state, {
            entries : state.entries.filter((e, i) => i !== castedAction.payload)
        });
    }

    return state;
}

