import { createStore, combineReducers } from 'redux';
import { entries } from '../reducers/entries';
import Entry from '../model/entry';
import { Store } from 'redux';

/* Storage shape */
export interface EntriesStorage{
    entries : Array<Entry>;
    currentDescription : string;
    currentAmount      : number;
} 

export interface RootStorage {
    entries : EntriesStorage
}

let appStore = createStore(combineReducers({ entries })) as Store<RootStorage>;
export { appStore as AppStore }; 