import React from 'react';
import {connect} from 'react-redux';
import Entry from '../model/entry';
import { RootStorage } from '../store/store';
import NumberFormat from 'react-number-format';
import { RemoveEntryAction } from '../actions/entries';
import _ from 'lodash';


function Currency(props : any){
    return (<NumberFormat value={props.value} 
        thousandSeparator="." 
        decimalSeparator=","
        displayType="text"
        fixedDecimalScale={true}
        decimalScale={2}
        suffix="€" />);
}

class Bill extends React.Component<any>{ 
    render() {
        let entries = this.props.entries;
        if (!entries || !entries.length){
            return <div className="alert alert-info" role="alert">No entries yet!</div>;
        }

        let rows = entries.map((e : Entry, index : number) => 
                                (<tr key={index}>
                                    <td className="col">{e.label}</td>
                                    <td className="col text-center">
                                        <Currency value={e.amount} />
                                    </td>
                                    <td className="col text-right">
                                        <i className="far fa-trash-alt" onClick={() => this.props.removeEntry(index)}></i>
                                    </td>
                                </tr>));

        let total = _.sumBy(entries, 'amount');
                                
        return (
            <table className="table table-striped">
                <thead className="thead-dark">
                    <tr>
                        <th className="col">Description</th>
                        <th className="col text-center">Price</th>
                        <th className="col text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                    <tr>
                        <td className="text-right" colSpan={3}>
                            Total: <Currency value={total} />
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    }
}

export default connect(
    (state : RootStorage, ownProps : any) => ({ entries : state.entries.entries || new Array<Entry>()}),
    {
        removeEntry : (index : number) => RemoveEntryAction.create(index)
    }
)(Bill);