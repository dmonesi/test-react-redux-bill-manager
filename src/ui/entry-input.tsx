import React from 'react';
import {AddEntryAction, DescriptionChangeAction, AmountChangeAction } from '../actions/entries';
import {connect} from 'react-redux';
import { RootStorage } from '../store/store';

class EntryInput extends React.Component<any>{
    render(){
        return (<div>
                    <div className="form-row">
                        <div className="col">
                            <input type="text" 
                                    name="description" 
                                    className="form-control"
                                    placeholder="Description"
                                    value={this.props.label}
                                    onChange={this.props.labelChanged}
                                    />
                            
                        </div>
                        <div className="col-3">
                            <input type="number" 
                                    name="price" 
                                    className="form-control"
                                    placeholder="Price"
                                    value={this.props.price}
                                    onChange={this.props.priceChanged}
                                    />
                        </div>
                        <div className="col-2">
                            <input type="submit" 
                                    value="Add"
                                    className="btn btn-primary btn-block"
                                    onClick={this.props.addEntry}/>
                        </div>
                    </div>
                </div>);
    }

}

export default connect(
    (state : RootStorage, ownProps : any) => ({ 
        label  : state.entries.currentDescription,
        price  : state.entries.currentAmount
    }),
    {
        labelChanged : (e : React.FormEvent<HTMLInputElement>) => DescriptionChangeAction.create(e.currentTarget.value),
        priceChanged : (e : React.FormEvent<HTMLInputElement>) => AmountChangeAction.create(parseFloat(e.currentTarget.value)),
        addEntry     : (e : React.MouseEvent<HTMLInputElement>) => AddEntryAction.create()
    }
)(EntryInput);