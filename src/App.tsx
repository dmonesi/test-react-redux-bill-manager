import './App.css';
import React, { Component } from 'react';
import Bill from './ui/bill';
import EntryInput from './ui/entry-input';


class App extends Component { 
  render() {
    return (
      <div className="container">
        <Bill />
        <hr />
        <EntryInput />
      </div>
    );
  }
}



export default App;
