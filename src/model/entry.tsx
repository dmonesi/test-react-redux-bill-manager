
export default class Entry {
    constructor(public label : string, 
                public amount : number,
                public created = new Date()){
    }
}
